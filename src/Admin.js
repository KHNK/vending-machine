// ### Admin
// * An `Admin` can load products into a `VendingMachine`'s compartments.
// * An `Admin` can set a price for a `VendingMachine`'s compartment.
// * An `Admin` can retrieve all of the money that the `VendingMachine` has received.
// * An `Admin` can count all the money he or she has retrieved.

class Admin {
  constructor() {
    this.__withdrawalBooks = {};
  }

  /**
   *
   * @param {*} vendingMachine
   * @param {string,number} product - object containing the name and price of the product
   * @param {number} quantity - the quantity of product to load
   *
   * @returns {quantity,name,price} - summary of this action, recording the product, price, and quantity successfully loaded
   */
  loadProducts(vendingMachine, { name, price }, quantity) {
    const { 
      compartmentId 
    } = vendingMachine.loadProducts(name, quantity);
    this.setCompartmentPrice(vendingMachine, compartmentId, price);

    return {
      quantity, name, price, compartmentId
    };
  }

  /**
   *
   * @param {*} vendingMachine
   * @param {number} compartmentId - the compartment to load this in
   * @param {number} price - the price to associate with this product
   */
  setCompartmentPrice(vendingMachine, compartmentId, price) {
    vendingMachine.setCompartmentPrice(compartmentId, price);
  }

  // technically there is an infinite supply of cash as we have infinite coins
  // If I was the owner of this machine, I would buy a ferrari with a million Sacagawea's
  /**
   * Withdraws all profits from this machine *ignoring the fact there is infinite suply*
   * and records it in this admins logs
   * @param {*} vendingMachine
   * @returns {number, uuid} - {profit,vendingMachineID} - returns the profit and the id of the vending machine we retrieved profits from
   */
  retrieveMoney(vendingMachine) {
    const moneyRetrieved = vendingMachine.retrieveMoney();
    const vendingMachineID = vendingMachine.getVendingMachineId();
    const currentMachineProfit = this.__withdrawalBooks[vendingMachineID] || 0;
    this.__withdrawalBooks[vendingMachineID] = currentMachineProfit + moneyRetrieved;
    return {
      profit: moneyRetrieved,
      vendingMachineID,
    };
  }

  /**
   * Returns the profit withdrawn from the machine, or the total list of all withdrawals awaiting deposit
   *
   * @param {*} vendingMachine - optional, the machine to check
   *
   * @returns {number | {uuid:number}} - returns the value of money retrieved for the given machine, or summary of withdrawals outstanding
   */
  countMoneyRetrieved(vendingMachine = false) {
    if (!vendingMachine) return Object.assign({}, this.__withdrawalBooks);
    const vendingMachineID = vendingMachine.getVendingMachineId();
    return this.__withdrawalBooks[vendingMachineID] || 0;
  }
}
module.exports = Admin;
