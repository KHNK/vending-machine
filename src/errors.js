class PenniesRequiredError extends Error {
  constructor() {
    super('Vending Machines require pennies for change');
  }
}

class InvalidCurrency extends Error {
  constructor(currency) {
    super(`Currency :${currency}: is not valid`);
  }
}

class CompartmentFull extends Error {
  constructor(compartmentId, productName) {
    super(`Not enough room in compartment ${compartmentId} for ${productName}`);
  }
}

module.exports = {
  PenniesRequiredError,
  InvalidCurrency,
  CompartmentFull,
};
