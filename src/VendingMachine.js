const { PenniesRequiredError, InvalidCurrency, CompartmentFull } = require('./errors');
const uuid = require('uuid/v4');
// ### VendingMachine
// * A `VendingMachine` has 10 compartments for different products.
// * Each compartment has 10 slots.
// * Each compartment has an associated price.
// * Each compartment will only hold one kind of product at a time.
// * A `VendingMachine` can display the price of a product.
// * A `VendingMachine` can receive money in the form of quarters, dimes, nickels, and pennies.
// * A `VendingMachine` can keep track of how much money it's received.
// * A `VendingMachine` can dispense a product from a selected compartment.
// * The `VendingMachine` must have received as much or more money than the product costs.
// * If it doesn't, it displays a message that says "Please add $<X>", where <X> is the difference between the received amount and the price.
// * The `VendingMachine` must have products remaining in the relevant compartment.
// * If it doesn't, it displays a message that says "Please make another selection."
// * The `VendingMachine` will dispense appropriate change using the smallest possible number of coins.
// * Assume that the `VendingMachine` has infinite coins for change-giving purposes.

// the currencies we accept in pennies;
// we only accept up to $5 bills
const currencyDenoms = [1, 5, 10, 25, 50, 100, 500];

class VendingMachine {
  constructor(numberCompartments = 10,
    numberCompartmentSlots = 10,
    currencyDenominations = currencyDenoms) {
    this.__currencyDenominations = currencyDenominations.sort((a, b) => (a - b)).reverse();
    this.__currentBalance = 0;
    this.__uniqueMachineId = new uuid();
    this.__compartmentCount = numberCompartments;
    this.__compartmentSlotCount = numberCompartmentSlots;

    const nonNumericCurrencies = currencyDenominations.filter(
      denomination => (typeof denomination !== 'number'),
    );
    if (nonNumericCurrencies.length) throw new InvalidCurrency('Non-Numeric Currency');

    // we want to simulate a real machine, so we want to ensure
    // the multidimensional array does not shift in size during use
    this.__compartments = {};
    for (let idx = 0; idx < this.__compartmentCount; idx++) {
      this.__compartments[idx] = {
        productName: undefined,
        price: 0,
        slots: ((slotCount) => {
          const slotArray = [];
          while (slotCount--) slotArray.push(undefined);
          return slotArray;
        })(this.__compartmentSlotCount),
      };
    }
    if (this.__isValidAmount(0)) throw new InvalidCurrency('0');
    if (!this.__isValidAmount(1)) throw new PenniesRequiredError();
  }

  /**
   * Checks to see whether or not the compartment exists
   *
   * @param {number} compartmentId - id of the compartment to check exists
   */
  __assertCompartmentIsValid(compartmentId) {
    if (compartmentId > this.__compartmentCount) {
      throw new Error(`Compartment ${compartmentId} does not exist`);
    }
    return true;
  }

  /**
   * Returns true if the compartment has all empty slots, false otherwise
   *
   * @param {*} compartmentId - id of the compartment to check
   *
   * @returns {boolean} - true or false depending of whether the compartment is empty or full
   */
  __isCompartmentEmpty(compartmentId) {
    const compartment = this.__compartments[compartmentId];
    const nonEmptySlots = compartment.slots.filter(slot => (typeof slot !== 'undefined'));
    return nonEmptySlots.length === 0;
  }

  /**
   * PRIVATE - for internal use by `VendingMachine` class ONLY
   * dispense the remaining balance
   *
   * @returns {number:number} - change map listing the denoms mapping to the quantity of the coin dispensed
   */
  __dispenseChange() {
    if (!this.__currentBalance) return {};
    const changeMap = {};
    let remainingChange = this.__currentBalance;

    for (const denomination of this.__currencyDenominations) {
      if (remainingChange / denomination) {
        const change = parseInt(remainingChange / denomination, 10);
        if (change === 0) continue;
        changeMap[denomination] = change;
        remainingChange %= denomination;
      }


      if (remainingChange === 0) break;
    }

    if (remainingChange !== 0) {
      throw new Error('Vending Machine did not have enough change');
    }
    this.__currentBalance = 0;
    return changeMap;
  }

  /**
   * PRIVATE - for internal use by `VendingMachine` class ONLY
   * Ensures a coin/bill is legit and supported by this machine
   * @param {number} amount - the amount to check
   */
  __isValidAmount(amount) {
    const isDenominationFound = this.__currencyDenominations.find(
      denomination => (amount === denomination),
    );
    return typeof isDenominationFound !== 'undefined';
  }

  /**
   * PRIVATE - for internal use by `VendingMachine` class ONLY
   *
   * loads a single instance of the product into the first free slot in the compartment
   *
   * @param {number} compartmentId
   * @param {string} name
   */
  __loadProduct(compartmentId, name) {
    const nextEmptySlotId = this.__compartments[compartmentId].slots.indexOf(undefined);
    if (nextEmptySlotId === -1) {
      throw new CompartmentFull(compartmentId, name);
    }
    this.__compartments[compartmentId].slots[nextEmptySlotId] = name;
    return name;
  }

  /**
   * Provides the user with the price associated with the compartmentId
   *
   * @param {number} compartmentId - the id of the compartment to price check
   */
  displayPrice(compartmentId) {
    this.__compartments[compartmentId];
  }

  /**
   * returns the current unspent balance of this machine
   * @returns {number} - current unspent balance loaded in the machine
   */
  getCurrentBalance() {
    return this.__currentBalance;
  }

  /**
   * Receives money supplied from the user. Ensures the amount is supported
   *
   * @param {number} amount - amount to deposit into machine
   *
   * @returns {number} currentBalance - current balance of this machine
   */
  receiveMoney(amount) {
    if (!amount) return;
    if (!this.__isValidAmount(amount)) {
      throw new Error('User attempted to use invalid currency');
    }
    this.__currentBalance += amount;

    return this.__curentBalance;
  }

  /**
   * dispenses a single item from the compartment id; maintains correct slot depth of the compartment
   * @param {*} compartmentId
   *
   * @returns {string, number} - object containing the product name dispensed and the change returned to the user
   */
  dispenseProduct(compartmentId) {
    const compartmentSlots = this.__compartments[compartmentId].slots;
    const product = compartmentSlots.shift();
    compartmentSlots.push(undefined);
    return {
      product,
      change,
    };
  }

  /**
   * Provides the unique id of this machine so the admin can track withdrawals
   *
   * @returns {uuid} - unique id enabling the admin to maintain logs of withdrawals
   */
  getVendingMachineId() {
    return this.__uniqueMachineId;
  }

  /**
   * Assigns the price to the compartment
   * @param {number} compartmentId
   * @param {number} price
   */
  setCompartmentPrice(compartmentId, price) {
    this.__compartments[compartmentId].price = price;
  }

  /**
   * Loads a compartment of this vending machine with a specific amount of a certain product
   *
   * @param {string} name - product name, this is the content of each compartment slot
   * @param {number} quantity - the amount of this product to load
   * @param {number} compartmentId - DEFAULTS -- the id of the compartment, defaults to the next available
   *
   * @returns {number,string,number} - summary of this action -- compartmentId, product name, quantity
   */
  loadProducts(name, quantity, compartmentId = this.__getFreeCompartment()) {
    if (compartmentId === false) throw new Error('All compartments are full');
    this.__assertCompartmentIsValid(compartmentId);
    if (!this.__isCompartmentEmpty(compartmentId)) {
      throw new Error(`Compartment: ${compartmentId} is not empty`);
    } else if (this.__compartmentCount < compartmentId) {
      throw new Error(`Compartment ${compartmentId} does not exist`);
    }

    this.set;
    while (quantity--) this.__loadProduct(compartmentId, name);
    return {
      compartmentId, name, quantity,
    };
  }
}
module.exports = VendingMachine;
