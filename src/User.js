// ### User
// * A `User` can check the price of a `VendingMachine`'s compartment.
// * A `User` can add money to a `VendingMachine`.
// * A `User` can buy a product from a `VendingMachine`, thereby receiving the product and the appropriate amount of change.
class User {
  checkPrice() {}

  addMoney() {}

  buyProduct() {}
}
module.exports = User;
