# Code Assessment: Vending Machine

Hello! Thanks for taking the time to complete this code assessment. Please read the entire document before starting.

## Problem Statement

You will create `VendingMachine`, `Admin`, and `User` classes that implement certain functionality and have certain properties. You will use those classes to complete a series of use cases, which will be described and executed by tests (in other words, no UI is required) - at the end of each test, assert that each "actor" in the use case has the correct state. For example, if a `User` buys something from a `VendingMachine`, the `User` should have the product and the proper change, and the `VendingMachine` should have the correct number of remaining products and the `User`'s money. You're free to use whatever testing harness and assertion library you wish. Let's get to it!