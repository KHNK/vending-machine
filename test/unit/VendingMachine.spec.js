const { assert } = require('chai');
const VendingMachine = require('src/VendingMachine');
const { PenniesRequiredError, InvalidCurrency, CompartmentFull } = require('src/errors');
// ### VendingMachine
// * A `VendingMachine` has 10 compartments for different products.
// * Each compartment has 10 slots.
// * Each compartment has an associated price.
// * Each compartment will only hold one kind of product at a time.
// * A `VendingMachine` can display the price of a product.
// * A `VendingMachine` can receive money in the form of quarters, dimes, nickels, and pennies.
// * A `VendingMachine` can keep track of how much money it's received.
// * A `VendingMachine` can dispense a product from a selected compartment.
// * The `VendingMachine` must have received as much or more money than
//     the product costs.
// * If it doesn't, it displays a message that says "Please add $<X>",
//    where <X> is the difference between the received amount and the price.
// * The `VendingMachine` must have products remaining in the relevant compartment.
// * If it doesn't, it displays a message that says "Please make another selection."
// * The `VendingMachine` will dispense appropriate change using the smallest
//    possible number of coins.
// * Assume that the `VendingMachine` has infinite coins for change-giving purposes.


describe('Test VendingMachine', () => {
  it('should be class', () => {
    assert.isObject(new VendingMachine());
  });
  it('should error out if we dont load the machine with pennies', () => {
    let vm;
    try {
      vm = new VendingMachine(10, 10, []);
      assert.fail(vm);
    } catch (e) {
      assert.instanceOf(e, PenniesRequiredError);
    }
  });

  it('should error out if we load the machine with non-numeric currencies', () => {
    let vm;
    try {
      vm = new VendingMachine(10, 10, ['fakemoney']);
      assert.fail(vm);
    } catch (e) {
      assert.instanceOf(e, InvalidCurrency);
    }
  });

  it('should error out if we load the machine with a 0 coin', () => {
    let vm;
    try {
      vm = new VendingMachine(10, 10, [0, 1]);
      assert.fail(vm);
    } catch (e) {
      assert.instanceOf(e, InvalidCurrency);
    }
  });

  /* PRIVATE METHODS */
  describe('-- test private methods', () => {
    it('should have necessary private methods', () => {
      const vm = new VendingMachine();
      assert.isFunction(vm.__dispenseChange);
      assert.isFunction(vm.__isValidAmount);
      assert.isFunction(vm.__assertCompartmentIsValid);
      assert.isFunction(vm.__isCompartmentEmpty);
      assert.isFunction(vm.__loadProduct);
    });

    /* __dispenseChange */
    it('should dispense no bills larger than 5 dollar bills', () => {
      const vm = new VendingMachine();
      vm.__currentBalance = 1000;
      const change = vm.__dispenseChange();
      assert.deepEqual(change, { 500: 2 });
    });
    it('should dispense the smallest amounts of change', () => {
      const vm = new VendingMachine();
      vm.__currentBalance = 41;
      const change = vm.__dispenseChange();
      assert.deepEqual(change, {
        25: 1,
        10: 1,
        5: 1,
        1: 1,
      });
    });

    /* __isValidAmount */
    it('should fail as default denoms does not have a 0 coin', () => {
      const vm = new VendingMachine();
      assert.isFalse(vm.__isValidAmount(0));
    });
    it('should pass as default denoms supports pennies', () => {
      const vm = new VendingMachine();
      assert.isTrue(vm.__isValidAmount(1));
    });

    /* __assertComponentIdIsValid */
    it('should throw error as vendingmachines by default only have 10 compartments', () => {
      const vm = new VendingMachine();
      try {
        // compartmentids start at 0 and increment by 1
        vm.__assertCompartmentIsValid(10);
      } catch (e) {
        assert.isError(e);
      }
    });
    it('should not error as compartmentId 9 is the last default compartment', () => {
      const vm = new VendingMachine();
      try {
        vm.__assertCompartmentIsValid(9);
      } catch (e) {
        assert.fail();
      }
    });

    /* __isCompartmentEmpty */
    // rely on checking they are not empty in __loadProduct test
    it('should pass as by default compartments are empty', () => {
      const vm = new VendingMachine();
      assert.isTrue(vm.__isCompartmentEmpty(0));
    });

    /* __loadProduct */
    it('should load a single item', () => {
      const vm = new VendingMachine();
      const product = 'test';
      const productLoaded = vm.__loadProduct(0, product);
      assert.equal(productLoaded, product);
    });
    it('should load an item and fail to load another with only 1 compartment slot', () => {
      const vm = new VendingMachine(1, 1);
      const productName = 'test';
      const compartmentId = 0;
      vm.__loadProduct(compartmentId, productName);
      const errorMessage = new CompartmentFull(compartmentId, productName).message;
      assert.throws(() => vm.__loadProduct(compartmentId, productName), errorMessage);
    });
  });


  /* PUBLIC METHODS */
  describe('-- test public methods', () => {
    it('should have expected public methods', () => {
      const vm = new VendingMachine();
      assert.isFunction(vm.displayPrice);
      assert.isFunction(vm.receiveMoney);
      assert.isFunction(vm.dispenseProduct);
      assert.isFunction(vm.getVendingMachineId);
      assert.isFunction(vm.setCompartmentPrice);
      assert.isFunction(vm.loadProducts);
    });

    /* displayPrice */
    /* receiveMoney */
    /* dispenseProduct */
  });
});
