const { assert } = require('chai');
const Admin = require('src/Admin');

// ### Admin
// * An `Admin` can load products into a `VendingMachine`'s compartments.
// * An `Admin` can set a price for a `VendingMachine`'s compartment.
// * An `Admin` can retrieve all of the money that the `VendingMachine` has received.
// * An `Admin` can count all the money he or she has retrieved.

const { spy } = require("sinon");

const mockVendingMachine = ()=>({
  loadProducts: spy((...args)=>(args)),
  setCompartmentPrice: spy((...args)=>(args)),
  retrieveMoney: spy(()=>500),
  getVendingMachineId: spy(()=>123)
});
describe('Test Admin', () => {
  it('should be a class', () => {
    assert.isObject(new Admin());
  });

  describe(" --- test private methods --- ", () => {
    it('should have necessary public methods', () => {
      const admin = new Admin();
      assert.isFunction(admin.loadProducts);
      assert.isFunction(admin.setCompartmentPrice);
      assert.isFunction(admin.retrieveMoney);
      assert.isFunction(admin.countMoneyRetrieved);
      assert.isObject(admin.__withdrawalBooks);
    });

    /* loadProducts */
    it("should orchestrate product load and price setting", () => {
      const mvm = mockVendingMachine();
      const admin = new Admin();
      const namePricePair = {
        name: 'product-name',
        price: 99
      };
      const quantity = 1;
      const {
        compartmentId 
      } = admin.loadProducts(mvm, namePricePair, quantity);
      assert.isTrue(mvm.loadProducts.calledWith(namePricePair.name, quantity));
      assert.isTrue(mvm.setCompartmentPrice.calledWith(compartmentId, namePricePair.price));
    });
    /* setCompartmentPrice */
    it("should set price for compartment", () => {
      const mvm = mockVendingMachine();
      const admin = new Admin();
      admin.setCompartmentPrice(mvm,1,100);
      assert.isTrue(mvm.setCompartmentPrice.calledWith(1, 100))
    });
    /* retrieveMoney */
    it("", () => {

    });
    /* countMoneyRetrieved */
    it("", () => {

    });
  });
});
