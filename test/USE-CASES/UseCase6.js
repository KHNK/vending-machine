const { assert } = require('chai');
const Admin = require('src/Admin');
const User = require('src/User');
const VendingMachine = require('src/VendingMachine');

// ### Use Case 6
// Setup: An Admin loads a VendingMachine with 2 Twixes (price: $.75 each), 2 Sour Patch Kids (price: $2.00 each), and 2 Atomic Warheads (price: $.50 each).
// * A user deposits enough money (in any denomination) to buy 2 Twixes and does so.
// * A different user deposits enough money to buy a Twix a tries to do so.
// * The second user realizes her error, checks the price of a Sour Patch Kids, adds enough money (in any denomination) to buy one, and does so.

describe('~~~ Use Case 6 ~~~', () => {});
