const { assert } = require('chai');
const Admin = require('src/Admin');
const User = require('src/User');
const VendingMachine = require('src/VendingMachine');
const {
  twix,
  twixPrice,
  sourPatchKids,
  sourPatchKidsPrice,
  atomicWarhead,
  atomicWarheadPrice,
} = require('./testConstants.json');

// ### Use Case 3
// Setup: An Admin loads a VendingMachine with 5 Twixes (price: $.75 each), 10 Sour Patch Kids (price: $2.00 each), and 3 Atomic Warheads (price: $.50 each).
// * A naive User deposits $.50 in dimes and tries to buy a Sour Patch Kids.

const dimeValue = 10;
const depositDimes = (user, vendingMachine, totalAmount) => {
  const count = totalAmount / dimeValue;
  while (count--) user.addMoney(vendingMachine, dimeValue);
};


describe('~~~ Use Case 3 ~~~', () => {
  it('should allow the admin to load the machine and the user to fail to buy spk', () => {
    const admin = new Admin();
    const user = new User();
    const vendingMachine = new VendingMachine();

    try {
      admin.loadProducts(vendingMachine, {
        name: twix,
        price: twixPrice,
      }, 5);
      admin.loadProducts(vendingMachine, {
        name: sourPatchKids,
        price: sourPatchKidsPrice,
      }, 10);
      admin.loadProducts(vendingMachine, {
        name: atomicWarhead,
        price: atomicWarheadPrice,
      }, 3);

      assert.isTrue(vendingMachine.hasProduct(twix));
      assert.isEqual(vendingMachine.checkPrice(twix), twixPrice);

      assert.isTrue(vendingMachine.hasProduct(sourPatchKids));
      assert.isEqual(vendingMachine.checkPrice(sourPatchKids), sourPatchKidsPrice);

      assert.isTrue(vendingMachine.hasProduct(atomicWarhead));
      assert.isEqual(vendingMachine.checkPrice(atomicWarhead), atomicWarheadPrice);

      const sourPatchKidsCompartment = vendingMachine.getProductCompartment(sourPatchKids);
      assert.isEqual(sourPatchKidsCompartment, 1);
      const price = user.checkPrice(vendingMachine, sourPatchKidsCompartment);
      assert.isEqual(price, sourPatchKidsPrice);
      depositDimes(user, vendingMachine, 50);
      user.buyProduct(vendingMachine, sourPatchKidsCompartment);
    } catch (e) {
      console.error(e);
      assert.isEqual(e.message, 'Please add $1.50');
    }
  });
});
