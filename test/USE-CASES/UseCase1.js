const { assert } = require('chai');
const Admin = require('src/Admin');
const User = require('src/User');
const VendingMachine = require('src/VendingMachine');
const {
  twix,
  twixPrice,
  sourPatchKids,
  sourPatchKidsPrice,
  atomicWarhead,
  atomicWarheadPrice,
} = require('./testConstants.json');
// ### Use Case 1
// Setup: An Admin loads a VendingMachine with 5 Twixes (price: $.75 each), 10 Sour Patch Kids (price: $2.00 each), and 3 Atomic Warheads (price: $.50 each).
// * A savvy User checks the price of a Twix.
// * That User deposits $.75 in quarters and buys a Twix.

describe('~~~ Use Case 1 ~~~', () => {
  it('should allow the admin to load the machine and the user to buy a twix', () => {
    const admin = new Admin();
    const user = new User();
    const vendingMachine = new VendingMachine();

    try {
      admin.loadProducts(vendingMachine, {
        name: twix,
        price: twixPrice,
      }, 5);
      admin.loadProducts(vendingMachine, {
        name: sourPatchKids,
        price: sourPatchKidsPrice,
      }, 10);
      admin.loadProducts(vendingMachine, {
        name: atomicWarhead,
        price: atomicWarheadPrice,
      }, 3);

      assert.isTrue(vendingMachine.hasProduct(twix));
      assert.isEqual(vendingMachine.checkPrice(twix), twixPrice);

      assert.isTrue(vendingMachine.hasProduct(sourPatchKids));
      assert.isEqual(vendingMachine.checkPrice(sourPatchKids), sourPatchKidsPrice);

      assert.isTrue(vendingMachine.hasProduct(atomicWarhead));
      assert.isEqual(vendingMachine.checkPrice(atomicWarhead), atomicWarheadPrice);

      const twixCompartment = vendingMachine.getProductCompartment(twix);
      assert.isEqual(twixCompartment, 0);
      const price = user.checkPrice(vendingMachine, twixCompartment);
      assert.isEqual(price, twixPrice);
      user.addMoney(vendingMachine, price);
      const { products, change } = user.buyProduct(vendingMachine, twixCompartment);
      assert.isEqual(changeReceived, 0);
    } catch (e) {
      console.error(e);
      assert.fail();
    }
  });
});
