const { assert } = require('chai');
const Admin = require('src/Admin');
const User = require('src/User');
const VendingMachine = require('src/VendingMachine');

// ### Use Case 5
// Setup: An Admin loads a VendingMachine with 5 Twixes (price: $.75 each), 10 Sour Patch Kids (price: $2.00 each), and 3 Atomic Warheads (price: $.50 each).
// * A generous User deposits $5 in quarters and walks away.
// * An Admin, having spied on the User, retrieves all the deposited money from the VendingMachine.
// * That Admin, sensing that he's in a tony area, raises the prices on all items by $.10.
// * A different User, who has only quarters, checks the price on a Twix, deposits enough money to buy 3, and does so.
// * That Admin retrieves all the deposited money from the VendingMachine and counts it.

describe('~~~ Use Case 5 ~~~', () => {});
