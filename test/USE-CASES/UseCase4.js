const { assert } = require('chai');
const Admin = require('src/Admin');
const User = require('src/User');
const VendingMachine = require('src/VendingMachine');
const {
  twix,
  twixPrice,
  sourPatchKids,
  sourPatchKidsPrice,
  atomicWarhead,
  atomicWarheadPrice,
} = require('./testConstants.json');

// ### Use Case 4
// Setup: An Admin loads a VendingMachine with 5 Twixes (price: $.75 each), 10 Sour Patch Kids (price: $2.00 each), and 3 Atomic Warheads (price: $.50 each).
// * A generous User deposits $5 in quarters and walks away.
// * A different (and lucky, and savvy) User checks the price of a Twix, of a Sour Patch Kids, and of an Atomic Warhead.
// * The second User buys two Atomic Warheads.

const quarterValue = 25;
const depositQuarters = (user, vendingMachine, totalAmount) => {
  const count = totalAmount / quarterValue;
  while (count--) user.addMoney(vendingMachine, quarterValue);
};

describe('~~~ Use Case 4 ~~~', () => {
  it('should allow the admin to load the machine and multiple users to transact', () => {
    const admin = new Admin();
    const user1 = new User();
    const user2 = new User();
    const vendingMachine = new VendingMachine();

    try {
      admin.loadProducts(vendingMachine, {
        name: twix,
        price: twixPrice,
      }, 5);
      admin.loadProducts(vendingMachine, {
        name: sourPatchKids,
        price: sourPatchKidsPrice,
      }, 10);
      admin.loadProducts(vendingMachine, {
        name: atomicWarhead,
        price: atomicWarheadPrice,
      }, 3);

      assert.isTrue(vendingMachine.hasProduct(twix));
      assert.isEqual(vendingMachine.checkPrice(twix), twixPrice);

      assert.isTrue(vendingMachine.hasProduct(sourPatchKids));
      assert.isEqual(vendingMachine.checkPrice(sourPatchKids), sourPatchKidsPrice);

      assert.isTrue(vendingMachine.hasProduct(atomicWarhead));
      assert.isEqual(vendingMachine.checkPrice(atomicWarhead), atomicWarheadPrice);

      depositQuarters(user1, vendingMachine, 500);

      const twixCompartment = vendingMachine.getProductCompartment(twix);
      assert.isEqual(twixCompartment, 0);

      const sourPatchKidsCompartment = vendingMachine.getProductCompartment(sourPatchKids);
      assert.isEqual(sourPatchKidsCompartment, 1);

      const atomicWarheadsCompartment = vendingMachine.getProductCompartment(atomicWarhead);
      assert.isEqual(atomicWarheadsCompartment, 2);

      const priceTwix = user2.checkPrice(vendingMachine, twixCompartment);
      assert.isEqual(priceTwix, twixPrice);
      const priceSPK = user2.checkPrice(vendingMachine, sourPatchKidsCompartment);
      assert.isEqual(priceSPK, sourPatchKidsPrice);
      const priceAtomicWarheads = user2.checkPrice(vendingMachine, atomicWarheadsCompartment);
      assert.isEqual(priceAtomicWarheads, atomicWarheadPrice);

      const change = user2.buyProduct(vendingMachine, sourPatchKidsCompartment, 2);
      assert.isEqual(change, 100);
    } catch (e) {
      console.error(e);
      assert.fail();
    }
  });
});
